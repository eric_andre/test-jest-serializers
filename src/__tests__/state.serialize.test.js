const serializeTreeMap = require('../../serializers/tree-map');
import initialState from '../mock/state.mock.js';

expect.addSnapshotSerializer(serializeTreeMap);

describe('State Map', () => {
  it('shoud match the snapshot', () => {
    expect(initialState).toMatchSnapshot();
  });

  it('should match updates', () => {
    const additionalState = Object.assign({}, initialState, {
      current: {
        surgery: {},
        item: {
          id: 54321,
          groups: [],
          preferences: {
            primary: {},
            backup: {},
          },
        },
      },
    });

    expect(additionalState).toMatchSnapshot();
  });
});
