const serializeHTMLTable = require('../../serializers/markdown-table');
expect.addSnapshotSerializer(serializeHTMLTable);

const mockTableComponent = (props) => (`
  <table markdown>
  <thead>
    <tr>
      <th>Firstname</th>
      <th>Lastname</th>
      <th>Age</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Jill</td>
      <td>Smith</td>
      <td>35</td>
    </tr>
    <tr>
      <td>Eve</td>
      <td>Jackson</td>
      <td>94</td>
    </tr>
  </tbody>
  </table>
`);

const mockInvalidComponent = (props) => (`
  <div class="table">
    <p>Not Valid Table</p>
  </div>
`);

describe('Table Markdown Serializer', () => {
  it('should maarkdown a table component', () => {
    expect(mockTableComponent()).toMatchSnapshot();
  });

  it('should not markdown html without table', () => {
    expect(mockInvalidComponent()).toMatchSnapshot();
  });
});
