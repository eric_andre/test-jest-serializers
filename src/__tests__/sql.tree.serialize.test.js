const serializeTreeMap = require('../../serializers/sql-tree');
expect.addSnapshotSerializer(serializeTreeMap);
import basicQuery from '../basic.sql';


describe('Tree map Serializer', () => {
  it('should serialize a component tree map', () => {
    expect(basicQuery).toMatchSnapshot();
  });
});
