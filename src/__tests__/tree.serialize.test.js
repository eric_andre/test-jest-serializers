const serializeTreeMap = require('../../serializers/tree-map');
expect.addSnapshotSerializer(serializeTreeMap);

import initialState from '../mock/state.mock.js';
const mockComponent = (props) => (`
  <nav>
    <ul>
      <li>Left</li>
      <li>Right</li>
    </ul>
  </nav>
`);

describe('Tree map Serializer', () => {
  it('should serialize a component tree map', () => {
    expect(mockComponent()).toMatchSnapshot();
  });

  it('should treeify the state/store', () => {
    expect(initialState).toMatchSnapshot();
  });
});
