import initialState from '../mock/state.mock.js';

describe('State Map', () => {
  it('shoud match the snapshot', () => {
    expect(initialState).toMatchSnapshot();
  });

  it('should match updates', () => {
    const additionalState = Object.assign({}, initialState, {
      current: {
        surgery: {},
        item: {
          id: 54321,
          groups: ['one', 'two', 'three'],
          preferences: {
            primary: {},
            backup: {},
          },
        },
      },
    });

    expect(additionalState).toMatchSnapshot();
  });
});
