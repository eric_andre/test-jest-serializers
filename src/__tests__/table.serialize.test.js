const serializeHTMLTable = require('../../serializers/html-table');
expect.addSnapshotSerializer(serializeHTMLTable);

const mockTableComponent = (props) => (`
  <table style="width:100%">
  <thead>
    <tr>
      <th>Firstname</th>
      <th>Lastname</th>
      <th>Age</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Jill</td>
      <td>Smith</td>
      <td>50</td>
    </tr>
    <tr>
      <td>Eve</td>
      <td>Jackson</td>
      <td>94</td>
    </tr>
  </tbody>
  </table>
`);

const mockInvalidComponent = (props) => (`
  <div class="table">
    <p>Not Valid Table</p>
  </div>
`);

describe('Table Serializer', () => {
  it('should serialize a table component', () => {
    expect(mockTableComponent()).toMatchSnapshot();
  });

  it('should not serialize html without table', () => {
    expect(mockInvalidComponent()).toMatchSnapshot();
  });
});
