import serializeSqlTable from '../../serializers/sql-table.js';
import basicQuery from '../basic.sql';

expect.addSnapshotSerializer(serializeSqlTable);

describe('Basic SQL query', () => {
  it('should output correctly', () => {
    expect(basicQuery).toMatchSnapshot();
  });

  it('should not try to sql serialize if not valid sql', () => {
    expect('<html>Not Valid SQL</html>').toMatchSnapshot();
  });
});
