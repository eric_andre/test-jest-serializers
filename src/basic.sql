SELECT
  t.id,
  t.description,
  t.name,
  o.other,
  so.prop
FROM test t
JOIN other o ON o.id = t.id
JOIN some so ON so.id = o.id
