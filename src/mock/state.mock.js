const initialState = {
'currentUser': {
  'user': {
    'isAuthenticated': false
  }
},
'preferences': {
  'current': null,
  'items': null
},
'kiosk': {
  'data': [],
  'current': null,
  'categories': null,
  'selectedAdminQuestions': []
},
'surgeries': {
  'data': [],
  'current': {
    'surgery': null,
    'isEditing': false,
    'isDraft': false
  },
  'implants': {},
  'requisitions': {
    'data': {},
    'notes': {}
  },
  'systems': null,
  'preferences': null,
  'payees': null,
  'isActiveFilter': true
},
'reports': {
  'current': {
    'report': []
  }
},
'calendar': {},
'sorting': {},
'errors': {},
'filtering': {},
'isFetching': {},
'referrer': null,
'pagination': {
  'surgeries': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'pricing_custom_rules': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'price_list_items': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'alerts_mappings': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'alerts_errors': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'client_physician_mappings': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'client_procedure_mappings': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'siu_messages': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'users': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'clients': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'system_clusters': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'scanned_items': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'items': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'smart_items': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'smart_items_history': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  },
  'survey_categories': {
    'isFetching': false,
    'pageCount': 1,
    'currentPage': 1,
    'resultsPerPage': 50,
    'totalRows': 1,
    'ids': []
  }
},
'enums': null,
'enumsFiltered': null,
'integrationTestTool': {
  'model': {},
  'resultMessages': [],
  'snapshots': {}
},
'surveyPackages': {
  'surveyPackageList': [],
  'surveyPackageFilterText': '',
  'currentSurveyPackage': {},
  'surveyPackageAvailableClients': [],
  'surveyPackageAvailablePhysicians': [],
  'masterSurveyList': [],
  'availableTemplates': []
},
'surgeryMessages': {
  'data': []
},
'surgeryOutcomes': {
  'data': [],
  'meta': {}
},
'surgeryNotifications': {},
'outcomePlayer': {
  'currentOutcome': {},
  'playlist': []
},
'notes': {
  'outcomes': null
},
'users': {
  'current': {},
  'physicians': [],
  'nurses': []
},
'clients': {
  'current': {
    'details': null,
    'physicianMappings': [],
    'procedureMappings': [],
    'promptOnUnload': false
  }
},
'appVersion': {
  'hash': 0
},
'pricing': {
  'rules': {
    'data': [],
    'current': null
  },
  'capitations': {
    'data': [],
    'current': null
  },
  'customRules': {
    'current': null,
    'priceLists': null
  },
  'scannedItem': null
},
'items': {
  'merge': {},
  'systemCluster': null,
  'manufacturers': {
    'current': null,
    'promptOnUnload': false
  },
  'priceLists': {
    'current': null,
    'items': null
  },
  'item': null
},
'data': {
  'siu_messages': [],
  'users': [],
  'clients': [],
  'reports': [],
  'move_survey': [],
  'preferences': [],
  'system_clusters': [],
  'manufacturers': [],
  'scanned_items': [],
  'items': [],
  'pricing_custom_rules': [],
  'kiosk_questions': [],
  'smart_items': [],
  'smart_items_history': [],
  'survey_categories': []
},
'integration': {
  'alerts': {
    'errors': [],
    'mappings': []
  }
},
'surveys': {
  'categories': {
    'current': {}
  }
}
};

module.exports = initialState;
