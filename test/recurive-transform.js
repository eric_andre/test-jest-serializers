const duckType = obj => Object.prototype.toString.call(obj);
const isObject = obj => obj && duckType(obj) === '[object Object]';
const isArray = obj => obj && duckType(obj) === '[object Array]';
const isFunction = fn => fn && duckType(fn) === '[object Function]';
const defaultOperator = (key, val, depth) => {
  return { [key]: val };
};

const traverse = (parent, operator, assignee = {}, depth = 0, isDone = false) => {
  console.log('Calling with  ', parent, ' - ', assignee, ' - ', depth);
  const assigned = Object.assign({}, assignee ? assignee : null);
  const keys = isObject(parent) && Object.keys(parent);
  const hasChildren = isObject(parent) && keys.length;
  const operate = isFunction(operator) ? operator : defaultOperator;
  const incrementedDepth = depth + 1;

  // if (isDone) {
  //   console.log('done', assigned);
  //   return assigned;
  // }

  if (isArray(parent)) {
    // check if children are objects
    const validChildren = parent.filter(item => isObject(item));
    console.log('Has ValidChildren', validChildren);
    validChildren.forEach((child, i) => {
      const childKey = Object.keys(child)[i];
      const transformed = operate(childKey, child[childKey], depth);

      traverse(Object.keys(child)[i], operator, transformed, incrementedDepth);
    })
  }

  if (hasChildren) {
    keys.forEach((key, i) => {
      const transformed = operate(key, parent[key], depth);

      traverse(parent[key], (key, val, depth) => operate(key, val, depth), Object.assign({}, assigned, transformed), incrementedDepth, i === keys.length - 1);
    });
  }
};

const transform = (obj, fn) => {
  let transformed = {};
  traverse(obj, (key, value, depth) => {
    const trans = fn(key, value);
    console.log('TRANS ', trans);
    transformed = Object.assign({}, transformed, trans); // [key] = value;
  })
  return transformed;
}

const operator = (key, val, depth) => {
  console.log('Operating on: ', key, ' value: ', val, ' depth: ', depth);
  return { [`${key}Transformed`]: val };
};

const mockObj = {
  name: 'jeremy',
  age: 33,
  friends: [
    { name: 'bob' }, { name: 'alice', skills: ['dev', 'games']}
  ],
  skills: {
    dev: {
      level: 1
    },
    games: {
      level: 4
    }
  }
};

const transformed = transform(mockObj, operator);

console.log('Transformed ', transformed);
