const HTML = require('html-parse-stringify');
const branches = require('../serializers/tree-branches');

const formatHTML = html => html && html.replace(/\s/g, '').replace(/[^\x00-\x7F]/g, '').trim()
const flatten = collection => collection.reduce(( acc, cur ) => acc.concat(cur), []);
const walk = operator => (parent, children = []) => (parent.children && parent.children.length)
  ? parent.children.map(child => walk(operator)(child, child.children))
  : operator(parent);

const isValidHTML = html => {
  const formattedHTML = formatHTML(html)
  try {
    HTML.parse(formattedHTML);
    return true;
  } catch(err) {
    return false;
  }

  return false
};

// currently only deals with the first table found in input
const formattedStr = formatHTML(input);

const tableAST = HTML.parse(formattedStr);

const walkOp = input => input.content;
const walked = walk(walkOp)(tableAST);
const flattened = flatten(walked);
const [colNames, ...rows] = flattened;
const mappedRows = rows.reduce((acc, curr) => {
  const colRow = colNames.map((col, i) => {
    const [colVal] = curr[i];
    return ({ [col]: colVal });
  });

  return acc.concat(Object.assign({}, ...colRow));
}, []);
