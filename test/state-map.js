const treeify = require('treeify');
const initialState = require('../src/mock/state.mock.js');
const branches = require('../serializers/tree-branches');

const duckType = (element) => Object.prototype.toString.call(element);
const isObject = (element) => duckType(element) === '[object Object]';

const treeRoot = `root ${branches.horizontal}`;

const renderHorizontal = (length, keys) => {
  let line = '';

  for (let i = 0; i < length * 2; i++) {
    line += branches.horizontal;
    if (i % 2 === 0) {
      const index = i / 2;
      const name = keys[index];
      line += `${branches.vertical} ${keys[index]}`;
    }
  }

  return line;
};

const depthOf = (object) => {
    var level = 1;
    var key;
    for(key in object) {
        if (!object.hasOwnProperty(key)) continue;

        if(typeof object[key] == 'object'){
            var depth = depthOf(object[key]) + 1;
            level = Math.max(depth, level);
        }
    }
    return level;
}

const makeTree = (parent, children = [], tree = treeRoot) => {
  const p = Array.isArray(parent) ? parent : Object.keys(parent).map(key => ({ [key]: parent[key] }));
  const isTail = Object.keys(parent).filter(key => !isObject(parent[key]));
  const rows = Array(depthOf(p)).fill([]);
  const cols = rows.map(() => Array(p.length).fill(''))
  console.log('P', p);
  if (p.length) {
    console.log('isObject', isObject(p));
    tree += renderHorizontal(p.length, Object.keys(parent));
    const pc = p;
    const parents = pc.filter(obj => isObject(obj));
    const children = pc.filter(obj => !isObject(obj));
    parents.forEach(obj => console.log(obj, ' isObject: ', isObject(obj)));
    console.log('Children ', pc);
    //return makeTree();
  }

  if (isTail.length) {
    isTail.forEach(node => {
      tree += node
    });
  }
  console.log('P depth ', cols);
  console.log('TREE ', tree);
};

const tree = makeTree(initialState);

console.log('TREE ', tree);
console.log('treeify ', treeify.asTree(initialState));
