DECLARE @strSplit VARCHAR = 'dbo.[splitString]';

SELECT pc.PhysicianId, pc.ClientId
FROM Physician_Client pc
WHERE pc.PhysicianId IN (SELECT Item FROM @strSplit(ISNULL(@PhysicianIds, ''), ','))
AND pc.ClientId IN (SELECT Item FROM @strSplit(ISNULL(@ClientIds, ''), ','))

