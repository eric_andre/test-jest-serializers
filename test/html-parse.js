const consoleTable = require('console.table');
const HTML = require('html-parse-stringify');
const markdownTable = require('markdown-table');

const formatHTML = html => html && html.replace(/\s/g, '').replace(/[^\x00-\x7F]/g, '').trim()

const tableHTML = `
<table>
  <thead>
    <tr>
      <th>Firstname</th>
      <th>Lastname</th>
      <th>Age</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Jill</td>
      <td>Smith</td>
      <td>50</td>
    </tr>
    <tr>
      <td>Eve</td>
      <td>Jackson</td>
      <td>94</td>
    </tr>
  </tbody>
</table>
<h1>HELLO</h1>
<table id='two' className="reactable"></table>
`;

const flatten = collection => collection.reduce(
  ( acc, cur ) => acc.concat(cur),
  []
);

const walk = operator => (parent, children = []) => (parent.children && parent.children.length)
  ? parent.children.map(child => walk(operator)(child, child.children))
  : operator(parent);

const formattedStr = formatHTML(tableHTML);

const tableAST = HTML.parse(formattedStr);
const filteredAST = tableAST.filter(node => node.name.includes('table'));
console.log('HTMLTABLE');
console.log('Filtered ', filteredAST)
// console.log('filtered ', filteredAST.forEach(element => {
//   console.log('EL ', element);
//   element.children.map(child => console.log('CHILD ', child))
// }));

// const reduced = filteredAST.reduce((acc, curr) => {
//   if (curr.children && curr.children.length) {
//     return acc.concat(curr);
//   }
//
//   return acc;
// }, []);

//console.log('REDUCED ', walk(filteredAST[0])(filteredAST[0].children));

console.log('ALLAST ', filteredAST);
const walkOp = input => input.content;
const walked = walk(walkOp)(filteredAST[0]);
const flattened = flatten(walked);
const [colNames, ...rows] = flattened;
console.log('OTHER ', rows)
console.table('walked ', walked);
console.log('COLNAMES ', colNames.map(([colName]) => colName));
const mappedRows = rows.reduce((acc, curr) => {
  const [firstname, lastname, age] = curr;
  const colRow = colNames.map((col, i) => {
    const [colVal] = curr[i];
    return ({ [col]: colVal });
  });

  return acc.concat(Object.assign({}, ...colRow));
}, []);
// console.log('flatWalk', flatten(walked).reduce((acc, curr) => {
//   return acc.concat(
//     curr.map()
//   );
// }, []))
//console.log('AS TABLE CONSOLE ', tableAST);
console.table(mappedRows)
console.log(markdownTable(flatten(walked)));
