require('require-sql');

const { Parser } = require('flora-sql-parser');
const groupBy = require('lodash/groupBy');
const parser = new Parser();
const branches = require('../serializers/tree-branches');


const isValidSQL = sql => {
  try {
    parser.parse(sql);
    return true;
  } catch(err) {
    return false;
  }
};
`
{ type: 'select',
options: null,
distinct: null,
columns:
 [ { expr: { type: 'column_ref', table: 't', column: 'description' },
   { expr: [Object], as: null },
   { expr: [Object], as: null },
   { expr: [Object], as: null } ],
from:
 [ { db: null, table: 'test', as: 't' },
   { db: null,
     table: 'other',
     as: 'o',
     join: 'INNER JOIN',
     on: [Object] } ],
where: null,
groupby: null,
having: null,
orderby: null,
limit: null }
`
const basicQuery = require('../src/basic.sql');
const queryAST = parser.parse(basicQuery);
const { columns, from, type, where } = queryAST;
// console.log('Query AST: ', queryAST);
console.log('Type ', type);
console.log('Col ', columns);
console.log('From ', from);
console.log('Where ', where);
console.log('BRANCES ', branches);

const groupedFrom = groupBy(from, 'as');
console.log('GROUPING ');
console.log('grouped from ', groupedFrom)
columns.forEach(col => {
  console.log('Column Table: ', col.expr.column, ' FROM ', groupedFrom[col.expr.table][0].table)
});
