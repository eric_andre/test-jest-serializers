'use strict';

require('require-sql');
const traverse = require('traverse');
const { Parser } = require('flora-sql-parser');
const parser = new Parser();

const basicQuery = require('../src/basic.sql');
const queryAst = parser.parse(basicQuery);

const traversed = traverse(queryAst).map(item => {
  console.log('ITEM ', item);
  return item;
});

console.log('SQL Traversed ', traversed);
