const traverse = require('traverse');

const mockObj = {
  name: 'jeremy',
  age: 33,
  friends: [
    { name: 'bob' }, { name: 'alice', skills: ['dev', 'games']}
  ],
  skills: {
    dev: {
      level: 1
    },
    games: {
      level: 4
    }
  }
};

const types = {
  object: '[object Object]',
  array: '[object Array]',
  func: '[object Function]',
  num: '[object Number]',
  bool: '[object Boolean]'
};
const duckType = obj => Object.prototype.toString.call(obj);
const isObject = obj => obj && duckType(obj) === types.object;
const getType = obj => Array.isArray(obj) ? 'array' : typeof obj;

const traversed = traverse(mockObj).map(item => {
  console.log('ITEM ', item);
  console.log('TYPE ', getType(item));
  if (isObject(item)) {
    return Object.keys(item).map(key => {
      return { [`${key}Traversed`]: item[key] };
    })
    .reduce((acc, curr) => Object.assign({}, acc, curr), {});
  }
  return item;
});

console.log('TRAVERSED ', traversed);
