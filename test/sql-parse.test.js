'use strict';

require('require-sql');
const consoleTable = require('console.table');
const { Parser } = require('flora-sql-parser');
const parser = new Parser();

const basicQuery = require('../src/basic.sql');
const queryAst = parser.parse(basicQuery);
const { columns, from } = queryAst;

console.log('FROM ', from)

const getTableFromAlias = (alias, from) => {
  return from.filter(({ as }) => as === alias)
    .reduce((acc, curr) => curr.table, '');
};

console.log('Columns');
const columnsTable = columns
  .map(({ expr: { column, table } }) => ({ column, table: getTableFromAlias(table, from) }))
  //.reduce((acc, curr) => Object.assign({}, acc, curr), {});
console.table(columnsTable);

columns.forEach(column => console.log('C: ', column));
console.log('Query AST: ', queryAst);
