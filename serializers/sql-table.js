const consoleTable = require('console.table');
const markdownTable = require('markdown-table')
const { Parser } = require('flora-sql-parser');
const parser = new Parser();

const isValidSQL = sql => {
  try {
    parser.parse(sql);
    return true;
  } catch(err) {
    return false;
  }
}

const getTableFromAlias = (alias, from) => from.filter(({ as }) => as === alias)
  .reduce((acc, curr) => curr.table, '');

module.exports = {
  test(input) {
    return input && isValidSQL(input);
  },
  print(input, serialize, indent) {
    const queryAst = parser.parse(input);
    const { columns, from } = queryAst;

    const tableHeader = ['Column', 'Table'];
    const columnRows = columns
      .map(({ expr: { column, table } }) => ([ column, getTableFromAlias(table, from)]))
    const sqlTable = [tableHeader, ...columnRows];
      // console.log('TABLECONSOLE ', `${ consoleTable.getTable(columnsTable)}`)
      // console.log('MARKDOWNTABLE ', markdownTable(sqlTable));//markdownTable(columnsTable));//
    return markdownTable(sqlTable); // consoleTable.getTable(columnsTable).trim();
  }
};
