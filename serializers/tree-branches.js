module.exports = {
  horizontal: '──',
  vertical: '│',
  down: '└── ',
  up: '┌── ',
  left: '┌── ',
  right: '└── '
};
