const markdownTable = require('markdown-table');
const HTML = require('html-parse-stringify');

const formatHTML = html => html && html.replace(/\s/g, '').replace(/[^\x00-\x7F]/g, '').trim()
const flatten = collection => collection.reduce(( acc, cur ) => acc.concat(cur), []);
const walk = operator => (parent, children = []) => (parent.children && parent.children.length)
  ? parent.children.map(child => walk(operator)(child, child.children))
  : operator(parent);


module.exports = {
  test(input) {
    return input && input.includes('<table markdown');
  },
  print(input) {
    // currently only deals with the first table found in input
    const formattedStr = formatHTML(input);

    const tableAST = HTML.parse(formattedStr);
    const filteredAST = tableAST.filter(node => node.name.includes('table'));
    const walkOp = input => input.content;
    const walked = walk(walkOp)(filteredAST[0]);

    return markdownTable(flatten(walked));
  }
};
