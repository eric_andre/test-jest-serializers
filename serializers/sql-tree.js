const { asTree } = require('treeify');
const { Parser } = require('flora-sql-parser');
const parser = new Parser();
const branches = require('./tree-branches');

const isValidSQL = sql => {
  try {
    parser.parse(sql);
    return true;
  } catch(err) {
    return false;
  }
}

module.exports = {
  test(input) {
    return input && isValidSQL(input);
  },
  print(input, serialize, indent) {
    const queryAst = parser.parse(input);
    const { columns } = queryAst;

    const columnsTable = columns
      .map(({ expr: { column, table } }) => ({ column, table: `${table}.${column}` }))

    return asTree(queryAst);
  }
};
